import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnDetailComponent } from './turn-detail.component';

describe('TurnDetailComponent', () => {
  let component: TurnDetailComponent;
  let fixture: ComponentFixture<TurnDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TurnDetailComponent]
    });
    fixture = TestBed.createComponent(TurnDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
