import { Component } from '@angular/core';

@Component({
  selector: 'app-reserved-date',
  templateUrl: './reserved-date.component.html',
  styleUrls: ['./reserved-date.component.css'],
})
export class ReservedDateComponent {
  showDetails: boolean = false;
  constructor() {}
  toggleDetails() {
    this.showDetails = !this.showDetails;
  }
}
