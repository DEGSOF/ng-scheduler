import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-available-date',
  templateUrl: './available-date.component.html',
  styleUrls: ['./available-date.component.css'],
})
export class AvailableDateComponent {
  @Input() day: string = '';
  @Input() time: string = '';
  //output to parent to add turn
  @Output() addTurnEmiter: EventEmitter<any> = new EventEmitter();
  addTurn() {
    //emit to parent
    this.addTurnEmiter.emit({ day: this.day, time: this.time });
  }
}
