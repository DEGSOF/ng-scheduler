import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit, AfterViewChecked {
  days: { title: string }[] = [];
  times: { title: string }[] = [];
  turns: { dayOfWeek: string; time: string; dated: string }[] = [];

  constructor(private cdref: ChangeDetectorRef) {
    this.generateTitles();
    this.generateTimes();
    this.turns = this.getTurns();
  }

  ngOnInit(): void {}

  ngAfterViewChecked(): void {
    this.cdref.detectChanges();
  }

  generateTitles() {
    const today = moment().format('YYYY-MM-DD');
    for (let i = 0; i < 3; i++) {
      const date = moment(today, 'YYYY-MM-DD')
        .add(i, 'days')
        .format('YYYY-MM-DD');
      this.days.push({
        title: date,
      });
    }
  }

  generateTimes() {
    for (let i = 0; i < 6; i++) {
      const time = moment().startOf('day').add(i, 'hours').format('HH:mm');
      this.times.push({
        title: time,
      });
    }
  }

  getTurns() {
    return [
      {
        dayOfWeek: 'Monday',
        time: '08:00',
        dated: '2023-12-03',
      },
      {
        dayOfWeek: 'Monday',
        time: '08:30',
        dated: '2023-12-03',
      },
      {
        dayOfWeek: 'Monday',
        time: '09:00',
        dated: '2023-12-03',
      },
      {
        dayOfWeek: 'Monday',
        time: '09:30',
        dated: '2023-12-03',
      },
      {
        dayOfWeek: 'Tuesday',
        time: '08:00',
        dated: '2023-12-07',
      },
      {
        dayOfWeek: 'Tuesday',
        time: '19:00',
        dated: '2023-12-09',
      },
    ];
  }

  addTurn(turn: { day: string; time: string }) {
    this.turns.push({
      dayOfWeek: 'Monday',
      time: turn.time,
      dated: turn.day,
    });
    this.turns = [...this.turns];
    this.cdref.detectChanges();
  }
}
