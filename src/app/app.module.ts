import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventsInDayPipe } from 'src/pipes/events-in-day.pipe';

import { AppComponent } from './app.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { TurnDetailComponent } from './components/turn-detail/turn-detail.component';
import { ReservedDateComponent } from './components/reserved-date/reserved-date.component';
import { AvailableDateComponent } from './components/available-date/available-date.component';

@NgModule({
  declarations: [AppComponent, ScheduleComponent, EventsInDayPipe, TurnDetailComponent, ReservedDateComponent, AvailableDateComponent],
  imports: [BrowserModule, BrowserAnimationsModule, MatTableModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
