import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'eventsInDay',
})
export class EventsInDayPipe implements PipeTransform {
  transform(
    turns: { dayOfWeek: string; time: string; dated: string }[],
    days: string,
    time: string
  ): any {
    for (const turn of turns) {
      if (turn) {
        if (turn.dated === days && turn.time === time) {
          return 'Reservado';
        }
      }
    }
  }
}
